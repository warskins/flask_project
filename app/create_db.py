"""
    The module is using to create tables in the  database from models,
     after transition a new database or removing all tables from the current.
"""
from app import db
from models import department, employee
db.create_all()
