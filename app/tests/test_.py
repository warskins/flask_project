from datetime import datetime
import pytest
from main import app
from app import db
from models.department import Department
from models.employee import Employee


# add to db one test department
@pytest.fixture
def department_bd_client():
    setup_db()
    department = Department(**{'name': "Finance"})
    db.session.add(department)
    db.session.commit()
    with app.test_client() as client:
        yield client


# use db with empty tables to test post request
@pytest.fixture
def empty_client():
    setup_db()
    with app.test_client() as client:
        yield client


# add to db 1 departments and 2 employees
@pytest.fixture()
def employee_db_client():
    setup_db()
    department = Department(**{'name': "Finance"})
    db.session.add(department)
    user = Employee(**{'first_name': 'Dima',
                       'last_name': 'Ivanov',
                       'patronymic': 'Yurievich',
                       'birthday': datetime.strptime('2000-06-16', "%Y-%m-%d"),
                       'salary': 25000,
                       'department_id': 1})
    user2 = Employee(**{'first_name': 'Andriy',
                        'last_name': 'Petrenko',
                        'patronymic': 'Ivanovich',
                        'birthday': datetime.strptime('1999-02-12', "%Y-%m-%d"),
                        'salary': 15000,
                        'department_id': 1})
    db.session.add(user)
    db.session.add(user2)
    db.session.commit()
    with app.test_client() as client:
        yield client


# test throwing exceptions. empty db, without tables
@pytest.fixture()
def bad_config():
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
    with app.test_client() as client:
        yield client


# test client to test web part of the application
@pytest.fixture
def client():
    with app.test_client() as client:
        yield client


# cleaning up db, after previous tests
def setup_db():
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
    db.drop_all()
    db.create_all()



