import json
from app import app
from tests.test_ import department_bd_client, empty_client


class TestDepartments:

    def test_departments_post(self, empty_client):
        # right request
        client = empty_client

        res = client.post('/api/departments',
                          data=json.dumps({'name': 'Finance'}),
                          content_type='application/json')
        assert res.status_code == 201
        assert res.headers['Content-Type'] == 'application/json'
        assert res.json['name'] == 'Finance'
        assert res.json['id'] == 1

        # request without json
        res = client.post('/api/departments')
        assert res.status_code == 400

        # request with invalid json
        res = client.post('/api/departments',
                          data='{}',
                          content_type='application/json')
        assert res.status_code == 400

        # request with wrong key
        res = client.post('/api/departments',
                          data=json.dumps({'f_name': 'Dima'}),
                          content_type='application/json')
        assert res.status_code == 400

    # get all departments test
    def test_departments_get_all(self, department_bd_client):
        client = department_bd_client
        res = client.get('/api/departments')
        assert res.status_code == 200
        assert res.headers['Content-Type'] == 'application/json'
        assert len(res.json) == 1

    # check with empty database
    def test_departments_get_all_empty(self, empty_client):
        client = empty_client
        res = client.get('/api/departments')
        assert res.status_code == 204
        assert res.json is None

    # get specific departments
    def test_departments_get(self, department_bd_client):
        client = department_bd_client
        res = client.get('/api/departments/1')
        assert res.status_code == 200
        assert res.headers['Content-Type'] == 'application/json'
        assert res.json['id'] == 1
        assert res.json['name'] == 'Finance'

        # test no existing user
        res = client.get('/api/departments/10')
        assert res.status_code == 404

        # test string type in parameters instead of int
        res = client.get('/api/departments/qwer')
        assert res.status_code == 404

        # test with id_department=0
        res = client.get('/api/departments/0')
        assert res.status_code == 404
        assert res.json['name'] == 'None'

    # change department  data
    def test_department_put(self, department_bd_client):
        client = department_bd_client
        # correct request

        res = client.put('/api/departments/1',
                         data=json.dumps({'name': 'IT'}),
                         content_type='application/json')
        assert res.status_code == 204
        # wrong department id(<0)
        res = client.put('/api/departments/0',
                         data=json.dumps({'name': 'IT'}),
                         content_type='application/json')
        assert res.status_code == 400

        # non-existent department
        res = client.put('/api/departments/10',
                         data=json.dumps({'name': 'IT'}),
                         content_type='application/json')
        assert res.status_code == 404

    # delete department test
    def test_department_delete(self, department_bd_client):
        client = department_bd_client
        res = client.delete('/api/departments/1')
        assert res.status_code == 204

        # non-existent department
        res = client.delete('/api/departments/2')
        assert res.status_code == 404

        # non-existent department
        res = client.delete('/api/departments/qwer')
        assert res.status_code == 404

        # wrong department id(<0)
        res = client.delete('/api/departments/0')
        assert res.status_code == 400



