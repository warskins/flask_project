import json
from tests.test_ import bad_config as client


class TestExceptions:

    # testing throwing exceptions in requests
    def test_exceptions_departments(self, client):
        # test delete
        res = client.delete('/api/departments/2')
        assert res.status_code == 400

        # test get_all
        res = client.get('/api/departments')
        assert res.status_code == 400

        # test get_one
        res = client.get('/api/departments/1')
        assert res.status_code == 400

        # test post
        res = client.post('/api/departments',
                          data=json.dumps({'name': 'Finance'}),
                          content_type='application/json'
                          )
        assert res.status_code == 400

        # test put
        res = client.put('/api/departments/1',
                         data=json.dumps({'name': 'IT'}),
                         content_type='application/json')
        assert res.status_code == 400

    # testing throwing exceptions in requests (employees)
    def test_exceptions_employees(self, client):
        # test delete
        res = client.delete('/api/employees/1')
        assert res.status_code == 400

        # # test get_all
        # res = client.get('/api/employees')
        # assert res.status_code == 400

        # test get_one
        res = client.get('/api/employees/1')
        assert res.status_code == 400

        # test post
        res = client.post('/api/employees',
                          data=json.dumps({'first_name': 'Dima'}),
                          content_type='application/json'
                          )
        assert res.status_code == 400

        # test put
        res = client.put('/api/employees/1',
                         data=json.dumps({'salary': 250000}),
                         content_type='application/json')
        assert res.status_code == 400
