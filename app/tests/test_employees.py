import json
from tests.test_ import empty_client, employee_db_client as client


class TestEmployee:

    # get all employees test
    def test_employees_get_all(self, client):
        res = client.get('/api/employees')
        assert res.status_code == 200
        assert res.headers['Content-Type'] == 'application/json'
        assert len(res.json) == 2
        assert res.json[0]['id'] == 1

    # test  with empty employees table
    def test_employees_get_all_empty(self, empty_client):
        res = empty_client.get('/api/employees')
        assert res.status_code == 204
        assert res.json is None

    # test  between two dates
    def test_employees_get_between_dates(self, client):
        # test exist employee
        res = client.get('/api/employees?from=2000-06-15&to=2000-06-17')
        assert res.status_code == 200
        assert res.headers['Content-Type'] == 'application/json'
        assert len(res.json) == 1

        # test non-existent employee or bad date type
        res = client.get('/api/employees?from=2012-02-12&to=2013-02-17')
        assert res.status_code == 204
        assert res.json is None

    # test  specific  date
    def test_employees_get_one_date(self, client):
        # test exist employee
        res = client.get('/api/employees?when=2000-06-16')
        assert res.status_code == 200
        assert res.headers['Content-Type'] == 'application/json'
        assert len(res.json) == 1

        # test non-existent employee or bad date type
        res = client.get('/api/employees?when=1992-06-16')
        assert res.status_code == 204
        assert res.json is None

    # test get one employee
    def test_employees_one(self, client):
        # test exist employee
        res = client.get('/api/employees/1')
        assert res.status_code == 200
        assert res.headers['Content-Type'] == 'application/json'
        assert res.json['id'] == 1

        # test non-existent employee
        res = client.get('/api/employees/10')
        assert res.status_code == 404

        # test incorrect id(<0)
        res = client.get('/api/employees/0')
        assert res.status_code == 400

    # test delete
    def test_employees_delete(self, client):
        # test exist employee
        res = client.delete('/api/employees/1')
        assert res.status_code == 204

        # test with id < 1(0)
        res = client.delete('/api/employees/0')
        assert res.status_code == 400

        # test non-existent employee
        res = client.delete('/api/employees/101')
        assert res.status_code == 400

    # test edit request
    def test_employees_edit(self, client):
        # correct request
        res = client.put('/api/employees/1',
                         data=json.dumps({'first_name': 'Yura',
                                          'salary': 34000}),
                         content_type='application/json')
        assert res.status_code == 204

        # request change 'birthday' date
        res = client.put('/api/employees/1',
                         data=json.dumps({'birthday': '2000-06-16'}),
                         content_type='application/json')
        assert res.status_code == 204

        # request with bad parameters
        res = client.put('/api/employees/1',
                         data=json.dumps({'birthday': 'dfggd'}),
                         content_type='application/json')
        assert res.status_code == 400

        # test with id < 1(0)
        res = client.put('/api/employees/0',
                         data=json.dumps({'first_name': 'Misha'}),
                         content_type='application/json')
        assert res.status_code == 400

        # un-existent employee
        res = client.put('/api/employees/111',
                         data=json.dumps({'first_name': 'Misha'}),
                         content_type='application/json')
        assert res.status_code == 404

    # test post request
    def test_employees_post(self, empty_client):
        client = empty_client
        # create employee
        res = client.post('/api/employees',
                          data=json.dumps(
                              {'first_name': 'Dima',
                               'last_name': 'Ivanov',
                               'patronymic': 'Yurievich',
                               'birthday': '2000-06-16',
                               'salary': 25000,
                               'department_id': 1}
                          ),
                          content_type='application/json')
        assert res.status_code == 201

        # empty request
        res = client.post('/api/employees',
                          content_type='application/json')
        assert res.status_code == 400

        # bad parameters
        res = client.post('/api/employees',
                          data=json.dumps(
                              {'first_name': 'Dima',
                               'last_name': 'Ivanov',
                               'patronymic': 'Yurievich',
                               'birthday': '2000-06-16',
                               'assas': 25000,   # incorrect key
                               }
                          ),
                          content_type='application/json')
        assert res.status_code == 400
