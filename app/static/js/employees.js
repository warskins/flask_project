 $(document).ready(function () {

            // load all employees and add them to table
            ajax_get_all('employees');

            // filter between two dates
            $('#filter_form, #form_current_date').on('submit', function (e) {
                e.preventDefault()
                $.ajax({
                    type: "GET",
                    url: '/api/employees?' + $(this).serialize(),
                    success: function (data) {
                        UpdateTable(data)
                    }
                });
            })

        });

//update table after ajax get
        function UpdateTable(data) {
            let table = $("#employees_table tbody");
            table.html('')
            $.each(data, function (idx, elem) {
                table.append("<tr><td>" + elem.id + `</td><td><a href='/departments/${elem.department_id}'>` + elem.department
                    + "</a></td>   <td>" + elem.first_name + "</td><td>" + elem.last_name
                    + "</td><td>" + elem.patronymic + "</td><td>" + elem.birthday
                    + "</td><td>" + elem.salary + `</td><td><a href='#' name='delete' id=${elem.id}>`
                    + 'Delete' + `</a></td><td><a name='update' href='#'  id=${elem.id}>` + 'Update' + "</a></td></tr>");
            });
        }