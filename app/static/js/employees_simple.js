$(document).ready(function (){
    //delete exist employee
            $(document).on('click', 'a[name="delete"]', function () {
                ajax_delete('employees', $(this).attr('id'))
            })

            $(document).on('click', 'a[name="update"]', function () {
                PopUpShowEdit()
                let id, department_id
                $.ajax({
                    url: '/api/employees/' + $(this).attr('id'),
                    type: 'GET',
                    success: function (data) {
                        id = data['id']
                        department_id = data['department_id']
                        UpdateDepartments(department_id)
                        $(' input[name="first_name"]').val(data['first_name'])
                        $('#edit_employee_form input[name="last_name"]').val(data['last_name'])
                        $('#edit_employee_form input[name="patronymic"]').val(data['patronymic'])
                        $('#edit_employee_form input[name="birthday"]').val(data['birthday'])
                        $('#edit_employee_form input[name="salary"]').val(data['salary'])


                    }
                })
                // update employee without submitting form
                $('#edit_employee_form').on('submit', function (e) {
                    e.preventDefault()
                    submit_form_put($(this),'employees', id)
                })

            })



            // add new employee without submitting form
            $('#add_employee_form').on('submit', function (e){
                e.preventDefault()
                submit_form_post($(this), 'employees')
            })
})



        //Show pop-up adding form
        function PopUpShowAdd() {
            $("#popup1").show();
            UpdateDepartments(0)
        }



        //Load all departments and insert them to add form
        function UpdateDepartments(selected) {
            let select = $("select[name='department_id']")
            select.html('')
            $.ajax({
                type: 'GET',
                url: "/api/departments",
                success: function (data) {
                    if (selected === 0) {
                        $.each(data, function (idx, elem) {
                            select.append(
                                `<option value='${elem.id}'>` + elem.name + '</option>'
                            )
                        })
                    } else {
                         $.each(data, function (idx, elem) {
                             if (selected !== elem.id){
                                 select.append(`<option value='${elem.id}'>` + elem.name + '</option>')
                             }else{
                                 select.append(`<option selected value='${elem.id}'>` + elem.name + '</option>')
                             }
                        })
                    }
                }
            })
        }
