from app import app
from views import view
from rest import api_departments, api_employees
from models import department, employee

if __name__ == '__main__':
    app.run()
