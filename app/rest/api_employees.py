import datetime
from sqlite3 import OperationalError

from flask import jsonify, request
from sqlalchemy.exc import SQLAlchemyError
from werkzeug.exceptions import BadRequestKeyError

from models.employee import Employee
from app import app, db, logger
from rest.api_departments import get_department


@app.route("/api/employees", methods=['POST'])
def add_employee():
    """ Add new employee to database

    Function operates with POST request from client.
    The request must contain a JSON object with all information about the new user.

    Returns status code 201, if request was correct and new entry has been added to database.

    """

    try:
        if 'birthday' in request.json:
            request.json['birthday'] = datetime.datetime.strptime \
                (request.json['birthday'], "%Y-%m-%d")
        user = Employee(**request.json)
        db.session.add(user)
        db.session.commit()
        logger.info('Added new employee:{} {} with id {}'
                    .format(user.first_name, user.last_name, user.id))
        return jsonify({"id": user.id,
                        "last_name": user.last_name}), 201

    except (SQLAlchemyError, ValueError, TypeError) as err:
        print(repr(err))
        logger.error(repr(err))
        return jsonify({'message:': 'Bad request'}), 400


@app.route('/api/employees/<int:_id>', methods=["PUT"])
def edit_employee(_id):
    """Update exist employee

    Use the variable _id to identify what user to update.
    Request object must contain a JSON object, with new information.
    If an employee with this id wasn't found, returns the message 'Not found.'

    """
    if _id < 1:
        return jsonify({"message": "Bad request"}), 400
    try:
        employee = Employee.query.filter(Employee.id == _id)
        if employee.count() == 0:
            return jsonify({"message": "Not found"}), 404

        # convert string format to datetime for sqlite3 if exist
        if 'birthday' in request.json:
            request.json['birthday'] = datetime.datetime.strptime \
                (request.json['birthday'], "%Y-%m-%d")

        employee.update(request.json)
        db.session.commit()
    except (SQLAlchemyError, TypeError, ValueError) as err:
        print(repr(err))
        logger.error(repr(err))
        return jsonify({"message": "Bad request"}), 400

    logger.info('Edited user with id:{}.'
                .format(_id))
    return jsonify(''), 204


@app.route('/api/employees/<int:_id>', methods=['DELETE'])
def delete(_id):
    """Delete existing employee from the database.

    Function operates with DELETE request from a client.
    Argument '_id' must contain an id of the employee to delete.
    If an employee with this id doesn't exist, returns a 404 status code
    and JSON object with the message 'Bad request'.

    """
    if _id < 1:
        return jsonify({"message": "Bad request"}), 400
    try:
        result = Employee.query.filter(Employee.id == _id).delete()
        if not result:
            return jsonify({'message:': 'Bad request'}), 400

        db.session().commit()
    except SQLAlchemyError as err:
        print(repr(err))
        logger.error(repr(err))
        return jsonify({'message:': 'Bad request'}), 400

    logger.info('Deleted employee with id:{}.'
                .format(_id))
    return jsonify(""), 204


@app.route('/api/employees', methods=['GET'])
def get_employees():
    """Get employees

    The return value depends on request parameters.
    There are 3 variants of request parameters.
    Firstly, when there are no parameters the  in request, returns
    full list of employees from the  database.
    Secondly, if there is one parameter, returns employees, who have
    birthday in the target date.
    Finally, the request can contain 2 parameters(from and to). In this way,
    returns employees who have birthdays between two given dates.

    """
    args = request.args
    try:
        if 'when' in args.keys():
            employees = Employee.query.filter(Employee.birthday == args['when'])
        elif 'from' in args.keys():
            employees = Employee.query.filter(Employee.birthday.between(args['from'], args['to']))
        else:
            employees = Employee.query

        if 'department_id' in args.keys():
            employees = employees.filter(Employee.department_id == args['department_id'])
    except (SQLAlchemyError, BadRequestKeyError) as err:

        print(repr(err))
        logger.error(repr(err))
        return jsonify({'message': 'Bad request'}), 400
    # .to_dict returns dict with department_id, but without department name
    #  .update insert another key 'department' with name of the current department
    res = []
    for employee in employees:
        employee_dep = get_department(employee.department_id)[0].json['name']
        employee = employee.to_dict()
        employee.update({'department': employee_dep})
        res.append(employee)

    if len(res) == 0:
        return '', 204

    return jsonify(res), 200


@app.route('/api/employees/<int:_id>', methods=['GET'])
def get_employee(_id):
    """Get information about specific employee

    Function processes GET request from client and require a unique id of the employee.
    If there are no problems with database connection,
    returns JSON object with information about this department.
    In case of error, returns the message 'Database error' and status code 500.
    When there is no employee in the database, it returns a message 'Not found' and status code 404.

    """
    if _id < 1:
        return jsonify({"message": "Bad request"}), 400
    try:
        employee = Employee.query.filter(Employee.id == _id).first()
    except SQLAlchemyError as err:
        print(repr(err))
        logger.error(repr(err))
        return jsonify({'message': 'Bad request'}), 400

    if employee is None:
        return jsonify({'message:': 'Not found'}), 404

    employee_dep = get_department(employee.department_id)[0].json['name']
    employee = employee.to_dict()
    employee.update({'department': employee_dep})

    return jsonify(employee), 200
